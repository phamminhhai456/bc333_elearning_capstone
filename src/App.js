import "./App.css";
import { ContainerApp, ContentApp, WrapperApp } from "./styles/App.Styled";
import { useDispatch } from "react-redux";
import { useEffect, useState } from "react";
import Header from "./components/Header/Header";
import { Route, Switch } from "react-router-dom";
import HomePage from "./pages/HomePage/HomePage";
import { actCheckLoginAsync } from "./redux/actions/authAction";
import { actGetListCategoriesAsync } from "./redux/actions/categoriesAction";
import { actGetListCourseAllAsync } from "./redux/actions/courseAction";
import { actGetCategoriesUserAsync } from "./redux/actions/userAction";
import LoginPage from "./pages/Login/LoginPage";
import RegisterPage from "./pages/Register/RegisterPage";
import CategoriesCourse from "./pages/CategoriesCourse/CategoriesCourse";
import SearchCourse from "./pages/SearchCourse/SearchCourse";
import Dashboard from "./pages/Dashboard/Dashboard";
import LoadingPage from "./components/LoadingPage/LoadingPage";
function App() {
  const dispatch = useDispatch();
  const [loading, setLoading] = useState(false);

  useEffect(
    function () {
      dispatch(actCheckLoginAsync());
    },
    [dispatch]
  );
  useEffect(
    function () {
      async function runPromiseAll() {
        setLoading(true);
        await dispatch(actGetListCategoriesAsync());
        await dispatch(actGetListCourseAllAsync());
        await dispatch(actGetCategoriesUserAsync());
        setLoading(false);
      }
      runPromiseAll();
    },

    [dispatch]
  );
  return (
    <WrapperApp>
      <ContainerApp>
        <Header />
        <ContentApp>
          <Switch>
            <Route path="/categories/:category" exact>
              <CategoriesCourse />
            </Route>
            <Route path="/search" exact>
              <SearchCourse />
            </Route>
            <Route path="/" exact>
              <HomePage />
            </Route>
            <Route path="/dev-courses">
              <HomePage />
            </Route>
            <Route path="/login">
              <LoginPage />
            </Route>
            <Route path="/register">
              <RegisterPage />
            </Route>
          </Switch>
          <Switch>
            <Route path="/dashboard">
              <Dashboard />
            </Route>
          </Switch>
        </ContentApp>
      </ContainerApp>
      <LoadingPage isLoading={loading} />
    </WrapperApp>
  );
}

export default App;
