import styled from "styled-components";

export const CommentStyle = styled.div`
  padding: 40px 100px 170px;
  position: relative;
  text-align: center;
  .title {
    font-size: 30px;
    color: #17388e;
  }
  .main-content {
    margin-top: 40px;
    .carousel {
      .carousel-inner {
        width: 90%;
        margin: 0 auto;
      }
      .content {
        line-height: 32px;
        font-style: italic;
        margin-bottom: 27px;
      }
      .info {
        font-style: italic;
        font-family: $font-title;

        span {
          color: black;
        }
      }
      .carousel-control-prev,
      .carousel-control-next {
        top: -65px;
        font-size: 50px;
        font-weight: 100;
        color: #000 !important;
        &:hover {
          color: #17388e !important;
        }
      }
      .carousel-control-prev {
        left: -80px;
      }
      .carousel-control-next {
        right: -80px;
      }
      .carousel-indicators {
        bottom: -85px;
        .img-info {
          position: relative;
          z-index: -2;
          margin: 0 -3px;
          border-radius: 50%;
          overflow: hidden;
          transform: scale(1);
          transition: all 0.2 linear;
          .overflow {
            position: absolute;
            top: 0;
            left: 0;
            width: 100%;
            height: 100%;
            background: rgba(0, 0, 0, 0.7);
          }
          &:first-child {
            z-index: -1;
          }
          &:last-child {
            z-index: -1;
          }
          &.active {
            z-index: 9999;
            transform: scale(1.1);
            .overflow {
              background: transparent;
            }
          }
        }
      }
    }
  }
`;
