import styled from "styled-components";

export const InfoCourseStyle = styled.div`
  background-attachment: fixed;
  background-size: cover;
  background-position: center center;
  position: relative;
  .ie-overflow {
    position: absolute;
    top: 0;
    left: 0;
    width: 100%;
    height: 100%;
    background: rgba(0, 0, 0, 0.75);
    background-position: center center;
    background-repeat: no-repeat;
    background-size: cover;
  }
  .ie-content {
    color: white;
    padding: 100px 50px;
    width: 100%;
    display: flex;
    justify-content: space-evenly;
    position: relative;
    .icon-group {
      text-align: center;
      letter-spacing: 1px;

      .amount {
        font-size: 60px;
      }
    }
  }
`;
