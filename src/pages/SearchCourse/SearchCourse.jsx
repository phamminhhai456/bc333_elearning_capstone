import { Affix, Result, Row } from "antd";
import queryString from "query-string";
import React, { useEffect, useState } from "react";
import { useDispatch } from "react-redux";
import { useLocation } from "react-router-dom";
import ListCourses from "../../components/Home/ListCourses";
import LoadingPaging from "../../components/Home/LoadingPaging";
import { actGetListCourseBySearchAsync } from "../../redux/actions/courseAction";
import { SpinStyled } from "../../styles/App.Styled";
import { HomeContainer } from "../../styles/Home.Styled";
import { ColResult } from "../../styles/Search.Styled";
import usePagingCourse from "../../utils/hook/usePagingCourse";

export default function SearchCourse() {
  const location = useLocation();
  const dispatch = useDispatch();
  const [searchText, setSearchText] = useState("");
  const [loadingFirst, setLoadingFirst] = useState(false);
  useEffect(
    function () {
      const cpp = location?.search.includes("++");
      if (cpp) {
        setSearchText(location?.search?.split("=")[1]);
      } else {
        setSearchText(queryString.parse(location.search)?.q);
      }
    },
    [location]
  );
  const {
    listCourses,
    totalPages,
    currentPage,
    loading,
    totalCount,
    renderButtonLoadMore,
  } = usePagingCourse({
    actAsync: actGetListCourseBySearchAsync,
    restParams: { tenKhoaHoc: searchText },
  });

  useEffect(
    function () {
      if (searchText) {
        setLoadingFirst(true);
        dispatch(
          actGetListCourseBySearchAsync({
            tenKhoaHoc: searchText,
          })
        ).finally(function () {
          setLoadingFirst(false);
        });
      }
    },
    [searchText, dispatch]
  );

  return (
    <HomeContainer style={{ marginTop: 10 }}>
      <SpinStyled spinning={loadingFirst} size="large">
        <Row style={{ paddingBottom: 10 }}>
          <ColResult span={24}>
            <Affix>
              <Result
                status={listCourses.length === 0 ? "500" : "success"}
                title={
                  listCourses.length !== 0
                    ? `Có ${totalCount} kết quả với "${searchText}"`
                    : `Không tìm thấy khóa học "${searchText}"`
                }
              />
            </Affix>
          </ColResult>
        </Row>

        {listCourses?.length !== 0 ? (
          <ListCourses listCourses={listCourses} />
        ) : null}
      </SpinStyled>
      {loading ? <LoadingPaging /> : null}
      {currentPage !== totalPages ? renderButtonLoadMore() : null}
    </HomeContainer>
  );
}
