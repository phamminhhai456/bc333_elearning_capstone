import { Col, message } from "antd";
import React, { useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { useHistory } from "react-router-dom";

import * as Yup from "yup";
import { Field, Form, Formik } from "formik";
import { AuthServices } from "../../services/auth.service";

export default function RegisterPage() {
  const dispatch = useDispatch();
  const history = useHistory();
  const [loading, setLoading] = useState(false);
  const [isHover, setIsHover] = useState(false);
  const labelCSS = {
    lineHeight: "1rem",
    border: "5px solid pink",
    padding: "12px 40px",
    "&.btn_edit": {
      borderRadius: "4px",
    },
    fontWeight: "600px",
    transition: "unset",
    backgroundColor: "#00a400",
    borderColor: isHover ? "transparent" : "#00a400",
    color: isHover ? "linear-gradient(#79bc64, #578843)" : "#fff",
  };
  const handleMouseEnter = () => {
    setIsHover(true);
  };

  const handleMouseLeave = () => {
    setIsHover(false);
  };
  let ivalid = false;
  const validationValues = (values) => {
    if (values.taiKhoan === "") {
      ivalid = false;
    } else if (values.email === "") {
      ivalid = false;
    } else if (values.soDT === "") {
      ivalid = false;
    } else if (values.matKhau === "") {
      ivalid = false;
    } else if (values.hoTen === "") {
      ivalid = false;
    } else if (values.maNhom === "") {
      ivalid = false;
    } else if (values.maLoaiNguoiDung === "") {
      ivalid = false;
    } else {
      ivalid = true;
    }
    return ivalid;
  };
  const SignupSchema = Yup.object().shape({
    taiKhoan: Yup.string().required("Tài khoản không được bỏ trống"),
    email: Yup.string()
      .required("Email không được bỏ trống")
      .email("Email sai định dạng"),
    soDT: Yup.string()
      .required("Số điện thoại không được bỏ trống")
      .matches(/^[0-9]+$/, "Số điện thoại không đúng")
      .min(10, "Số điện thoại ít nhất là 10")
      .max(11, "Và nhiều nhất là 11"),
    matKhau: Yup.string()
      .required("Mật khẩu không được bỏ trống")
      .min(8, "Mật khẩu tối thiểu 8 ký tự")
      .max(32, "Mật khẩu tối đa 32 ký tự")
      .matches(
        /^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[^a-zA-Z0-9])(?!.*\s).{0,}$/,
        "Mật khẩu phải bao gồm ít nhất 1 ký tự viết hoa, 1 ký tự số, 1 ký tự đặc biệt và chữ"
      ), //.test(/cybersof/,'Mật khẩu không đúng định dạng')
    hoTen: Yup.string()
      .required("Họ Tên Không Được Bỏ Trống")
      .matches(
        "^[a-zA-Z_ÀÁÂÃÈÉÊẾÌÍÒÓÔÕÙÚĂĐĨŨƠàáâãèéêìíòóôõùúăđĩũơƯĂẠẢẤẦẨẪẬẮẰẲẴẶ" +
          "ẸẺẼỀỀỂưăạảấầẩẫậắằẳẵặẹẻẽềềểếỄỆỈỊỌỎỐỒỔỖỘỚỜỞỠỢỤỦỨỪễệỉịọỏốồổỗộớờởỡợ" +
          "ụủứừỬỮỰỲỴÝỶỸửữựỳỵỷỹ\\s]+$",
        "Họ tên phải là chữ"
      ),
    maNhom: Yup.string().required("Vui Lòng Chọn Mã Nhóm"),
    maLoaiNguoiDung: Yup.string().required("Vui Lòng Chọn Loại Người Dùng"),
  });
  return (
    <div className="mt-3">
      <Formik
        initialValues={{
          taiKhoan: "",
          matKhau: "",
          hoTen: "",
          soDT: "",
          maNhom: "",
          email: "",
          maLoaiNguoiDung: "",
        }}
        validationSchema={SignupSchema}
        onSubmit={(values) => {}}
      >
        {({ errors, touched, values, resetForm }) => (
          <Form className="container">
            <div className="row flex-column align-items-center justify-content-center">
              <h3>Đăng Ký</h3>

              <div id="textTitleInput" className="col-6">
                <div className="form-group">
                  <span>Tài Khoản</span>
                  <Field className="form-control" name="taiKhoan" />
                  {errors.taiKhoan && touched.taiKhoan ? (
                    <div className="alert alert-danger">{errors.taiKhoan}</div>
                  ) : null}
                </div>
                <div className="form-group">
                  <span>Họ Và Tên</span>
                  <Field className="form-control" name="hoTen" />
                  {errors.hoTen && touched.hoTen ? (
                    <div className="alert alert-danger">{errors.hoTen}</div>
                  ) : null}
                </div>
                <div className="form-group">
                  <span>Số Điện Thoại</span>

                  <Field className="form-control" name="soDT" />
                  {errors.soDT && touched.soDT ? (
                    <div className="alert alert-danger">{errors.soDT}</div>
                  ) : null}
                </div>
                <div className="form-group">
                  <span>Mã Nhóm</span>
                  <Field className="form-control" as="select" name="maNhom">
                    <option value="">Chọn Mã Nhóm</option>
                    <option>GP01</option>
                    <option>GP02</option>
                    <option>GP03</option>
                    <option>GP04</option>
                    <option>GP05</option>
                    <option>GP06</option>
                    <option>GP07</option>
                    <option>GP08</option>
                    <option>GP09</option>
                    <option>GP10</option>
                    <option>GP11</option>
                    <option>GP12</option>
                    <option>GP13</option>
                    <option>GP14</option>
                    <option>GP15</option>
                  </Field>
                  {errors.maNhom && touched.maNhom ? (
                    <div className="alert alert-danger">{errors.maNhom}</div>
                  ) : null}
                </div>
                <div className="form-group">
                  <span>Mã Loại Người Dùng</span>
                  <Field
                    className="form-control"
                    as="select"
                    name="maLoaiNguoiDung"
                  >
                    <option value="">Mã Loại Người Dùng</option>

                    <option>GV</option>

                    <option>HV</option>
                  </Field>

                  {errors.maLoaiNguoiDung && touched.maLoaiNguoiDung ? (
                    <div className="alert alert-danger">
                      {errors.maLoaiNguoiDung}
                    </div>
                  ) : null}
                </div>
                <div className="form-group">
                  <span>Email</span>
                  <Field className="form-control" name="email" type="email" />
                  {errors.email && touched.email ? (
                    <div className="alert alert-danger">{errors.email}</div>
                  ) : null}
                </div>
                <div className="form-group">
                  <span>Mật Khẩu</span>
                  <Field
                    className="form-control"
                    type="password"
                    name="matKhau"
                  />
                  {errors.matKhau && touched.matKhau ? (
                    <div className="alert alert-danger">{errors.matKhau}</div>
                  ) : null}
                </div>
                <div className="text-center">
                  <button
                    style={labelCSS}
                    onMouseEnter={handleMouseEnter}
                    onMouseLeave={handleMouseLeave}
                    onClick={() => {
                      validationValues(values);
                      if (ivalid === true) {
                        let handleRegister = async (values) => {
                          try {
                            let res = await AuthServices.Register(values);
                            console.log(res);
                            message.success("Đăng kí thành công");
                            history.push("/login");
                          } catch (err) {
                            message.error("Đăng Kí thất bại");
                            console.log(err);
                            if (err.reponse.data === "Email đã tồn tại!") {
                              message.error("Email đã tồn tại!");
                            } else {
                              message.error("Tài khoản đã tồn tại!");
                            }
                          }
                        };
                        handleRegister(values);
                      } else {
                        message.error("Đăng Kí thất bại");
                      }
                    }}
                    className="btn buttonAddtionMem"
                    type="submit"
                  >
                    Đăng Ký
                  </button>
                </div>
              </div>
            </div>
          </Form>
        )}
      </Formik>
    </div>
  );
}
