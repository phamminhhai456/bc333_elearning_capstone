import React, { useEffect, useState } from "react";
import { useDispatch } from "react-redux";
import useAuth from "../../utils/hook/useAuth";
import {
  actGetAllUserAsync,
  actGetUserPagingAsync,
} from "../../redux/actions/userAction";
import { Col, Row } from "antd";
import LoadingPage from "../../components/LoadingPage/LoadingPage";
import DashboardMenu from "../../components/Dashboard/DashboardMenu";
import DashboardManager from "../../components/Dashboard/DashboardManager";
export default function Dashboard() {
  useAuth();
  const dispatch = useDispatch();
  const [loading, setLoading] = useState(false);

  useEffect(
    function () {
      async function runAll() {
        setLoading(true);
        await dispatch(actGetUserPagingAsync());
        await dispatch(actGetAllUserAsync());
        setLoading(false);
      }

      runAll();
    },
    [dispatch]
  );
  return (
    <>
      <Row style={{ height: "100%" }}>
        <Col md={4} xs={24}>
          <DashboardMenu />
        </Col>
        <Col md={20} xs={24}>
          <DashboardManager />
        </Col>
      </Row>
      <LoadingPage isLoading={loading} />
    </>
  );
}
