import React, { useState } from "react";
import { useDispatch } from "react-redux";
import { NavLink, useHistory } from "react-router-dom";
import {
  showNotification,
  typeNotify,
} from "../../components/shared/Notification";
import useNotAuth from "../../utils/hook/useNotAuth";

import * as Yup from "yup";

import { message } from "antd";

import { actLoginAsync } from "../../redux/actions/authAction";
import { Field, Form, Formik } from "formik";
export default function LoginPage() {
  useNotAuth();
  const dispatch = useDispatch();
  const history = useHistory();
  const [loading, setLoading] = useState(false);
  const [isHover, setIsHover] = useState(false);
  const labelCSS = {
    lineHeight: "1rem",
    border: "5px solid pink",
    padding: "12px 40px",
    "&.btn_edit": {
      borderRadius: "4px",
    },
    fontWeight: "600px",
    transition: "unset",
    backgroundColor: "#00a400",
    borderColor: isHover ? "transparent" : "#00a400",
    color: isHover ? "linear-gradient(#79bc64, #578843)" : "#fff",
  };
  const handleMouseEnter = () => {
    setIsHover(true);
  };

  const handleMouseLeave = () => {
    setIsHover(false);
  };
  let ivalid = false;
  const validationValues = (values) => {
    if (values.taiKhoan === "") {
      ivalid = false;
    } else if (values.matKhau === "") {
      ivalid = false;
    } else {
      ivalid = true;
    }
    return ivalid;
  };
  const SignupSchema = Yup.object().shape({
    taiKhoan: Yup.string().required("Tài khoản không được bỏ trống"),
    matKhau: Yup.string().required("Mật khẩu không được bỏ trống"),
  });
  return (
    <div className="mt-3">
      <Formik
        initialValues={{
          taiKhoan: "",
          matKhau: "",
        }}
        validationSchema={SignupSchema}
        onSubmit={(values) => {}}
      >
        {({ errors, touched, values, resetForm }) => (
          <Form className="container">
            <div className="row flex-column align-items-center justify-content-center">
              <h3>Đăng Nhập</h3>

              <div id="textTitleInput" className="col-6">
                <div className="form-group">
                  <span>Tài Khoản</span>
                  <Field className="form-control" name="taiKhoan" />
                  {errors.taiKhoan && touched.taiKhoan ? (
                    <div className="alert alert-danger">{errors.taiKhoan}</div>
                  ) : null}
                </div>
                <div className="form-group">
                  <span>Mật Khẩu</span>
                  <Field
                    className="form-control"
                    type="password"
                    name="matKhau"
                  />
                  {errors.matKhau && touched.matKhau ? (
                    <div className="alert alert-danger">{errors.matKhau}</div>
                  ) : null}
                </div>
                <div className="text-center">
                  <button
                    style={labelCSS}
                    onMouseEnter={handleMouseEnter}
                    onMouseLeave={handleMouseLeave}
                    onClick={() => {
                      validationValues(values);
                      if (ivalid === true) {
                        dispatch(actLoginAsync(values)).then(function (res) {
                          setLoading(false);
                          if (res.ok) {
                            showNotification({
                              type: typeNotify.success,
                              message: "Thành công",
                              description: `Tài khoản ${res?.user?.taiKhoan} đăng nhập thành công`,
                              duration: 3,
                            });
                            history.push("/");
                          } else {
                            showNotification({
                              type: typeNotify.error,
                              message: "Đăng nhập thất bại",
                              description: res?.message || "",
                              duration: 3,
                            });
                          }
                        });
                      } else {
                        message.error("Đăng Nhập thất bại");
                      }
                    }}
                    className="btn buttonAddtionMem"
                    type="submit"
                  >
                    Đăng Nhập
                  </button>
                </div>
                <div className="text-center mt-3">
                  Bạn chưa có tài khoản ?
                  <NavLink className="ml-3" to="/register">
                    Đăng Ký Ngay
                  </NavLink>
                </div>
              </div>
            </div>
          </Form>
        )}
      </Formik>
    </div>
  );
}
