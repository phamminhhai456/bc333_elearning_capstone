import React, { useEffect } from "react";
import { useDispatch } from "react-redux";
import { actGetListCourseAsync } from "../../redux/actions/courseAction";
import usePagingCourse from "../../utils/hook/usePagingCourse";
import { ExclamationCircleOutlined } from "@ant-design/icons";
import {
  AlertStyled,
  HomeContainer,
  RowTabsListCourse,
} from "../../styles/Home.Styled";
import { SpacingStyled } from "../../styles/App.Styled";
import { Affix, Col } from "antd";
import BackgroupHome from "../../components/Home/BackgroupHome";
import TopCompanies from "../../components/Home/TopCompanies";
import IntroductionCourses from "../../components/Home/IntroductionCourses";
import ListCourses from "../../components/Home/ListCourses";
import LoadingPaging from "../../components/Home/LoadingPaging";
import TabListCourseCategory from "../../components/TabListCourseCategory/TabListCourseCategory";
import PopularTopics from "../../components/PopularTopics/PopularTopics";
import BackToTop from "../../components/BackToTop/BackToTop";
import InfoCourse from "../../components/Home/InfoCourse";
import Comment from "../../components/Home/Comment";
import BuyCourse from "../../components/Home/BuyCourse";
import Footer from "../../components/Footer/Footer";
export default function HomePage() {
  const dispatch = useDispatch();
  const {
    listCourses,
    totalPages,
    currentPage,
    loading,
    renderButtonLoadMore,
  } = usePagingCourse({ actAsync: actGetListCourseAsync });

  useEffect(
    function () {
      dispatch(actGetListCourseAsync());
    },
    [dispatch]
  );
  return (
    <>
      <HomeContainer>
        <BackgroupHome />
        <SpacingStyled sizeSpacing="30px" />
        <RowTabsListCourse>
          <Col span={24} style={{ padding: "0 8px" }}>
            <TabListCourseCategory />
          </Col>
        </RowTabsListCourse>
        <SpacingStyled sizeSpacing="30px" />
        <PopularTopics />
        <SpacingStyled sizeSpacing="30px" />
        <TopCompanies />
        <SpacingStyled sizeSpacing="40px" />
        <IntroductionCourses />
        <SpacingStyled sizeSpacing="30px" />
        {/* List Course Paging */}
        <Affix offsetTop={0}>
          <AlertStyled
            type="info"
            showIcon
            icon={<ExclamationCircleOutlined />}
            banner={true}
            message="Do ảnh hưởng của dịch Covid-19, DevCourse miễn phí tất cả các khóa học !"
          />
        </Affix>
        <ListCourses listCourses={listCourses} />
        {loading ? <LoadingPaging /> : null}
        {currentPage !== totalPages ? renderButtonLoadMore() : null}
        <SpacingStyled sizeSpacing="30px" />
        <InfoCourse />
        <SpacingStyled sizeSpacing="30px" />
        <BuyCourse />
        <Comment />
        <BackToTop />
      </HomeContainer>
      <Footer />
    </>
  );
}
