import React from "react";
import { CommentStyle, CommentTitle } from "../../styles/Comment.Styled";

export default function Comment() {
  return (
    <CommentStyle>
      <h2 className="title">REVIEW</h2>
      <div className="main-content">
        <div id="cmt-user" className="carousel slide" data-ride="carousel">
          <div className="carousel-indicators">
            <div
              className="img-info active"
              data-target="#cmt-user"
              data-slide-to={0}
            >
              <div className="overflow"></div>
              <img src="./images/cmt-1.jpg" />
            </div>
            <div className="img-info" data-target="#cmt-user" data-slide-to={1}>
              <div className="overflow"></div>
              <img src="./images/cmt-2.jpg" />
            </div>
            <div className="img-info" data-target="#cmt-user" data-slide-to={2}>
              <div className="overflow"></div>
              <img src="./images/cmt-3.jpg" />
            </div>
          </div>
          <div className="carousel-inner">
            <div className="carousel-item active">
              <div className="content">
                Lorem ipsum dolor sit amet consectetur adipisicing elit.
                Quisquam explicabo, accusamus ipsum maiores aliquam modi
                reprehenderit maxime quam dolorem suscipit quasi voluptates aut
                soluta sunt tempora odio minima ex sint optio perferendis
                aspernatur eum? Amet beatae cum itaque recusandae, nobis, ab
                doloremque dolore quibusdam laudantium sit at fugiat voluptate?
                Ex.
              </div>
              <div className="info">
                <p>
                  Luna, <span>Front End</span>
                </p>
              </div>
            </div>
            <div className="carousel-item">
              <div className="content">
                Lorem ipsum dolor sit amet consectetur adipisicing elit.
                Quisquam explicabo, accusamus ipsum maiores aliquam modi
                reprehenderit maxime quam dolorem suscipit quasi voluptates aut
                soluta sunt tempora odio minima ex sint optio perferendis
                aspernatur eum? Amet beatae cum itaque recusandae, nobis, ab
                doloremque dolore quibusdam laudantium sit at fugiat voluptate?
                Ex.
              </div>
              <div className="info">
                <p>
                  Ginny, <span>Back end</span>
                </p>
              </div>
            </div>
            <div className="carousel-item">
              <div className="content">
                Lorem ipsum dolor sit amet consectetur adipisicing elit.
                Quisquam explicabo, accusamus ipsum maiores aliquam modi
                reprehenderit maxime quam dolorem suscipit quasi voluptates aut
                soluta sunt tempora odio minima ex sint optio perferendis
                aspernatur eum? Amet beatae cum itaque recusandae, nobis, ab
                doloremque dolore quibusdam laudantium sit at fugiat voluptate?
                Ex.
              </div>
              <div className="info">
                <p>
                  Hermione, <span>Mobile</span>
                </p>
              </div>
            </div>
          </div>
          <a
            className="carousel-control-prev"
            href="#cmt-user"
            role="button"
            data-slide="prev"
          >
            <span>{"<"}</span>
          </a>
          <a
            className="carousel-control-next"
            href="#cmt-user"
            role="button"
            data-slide="next"
          >
            <span>{">"}</span>
          </a>
        </div>
      </div>
    </CommentStyle>
  );
}
