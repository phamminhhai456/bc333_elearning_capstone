import { Col } from "antd";
import React from "react";
import { RowListCoursePaging } from "../../styles/Home.Styled";
import CourseItem from "../Courses/CourseItem";

export default function ListCourses({ listCourses }) {
  return (
    <div className="container_list-course">
      <RowListCoursePaging
        gutter={[
          { xs: 8, sm: 8, md: 16, lg: 24 },
          { xs: 8, sm: 8, md: 16, lg: 24 },
        ]}
        style={{ marginLeft: 0, marginRight: 0 }}
      >
        {listCourses.length !== 0
          ? listCourses.map(function (course, index) {
              return (
                <Col xs={12} md={8} lg={6} key={index}>
                  <CourseItem course={course} />
                </Col>
              );
            })
          : null}
      </RowListCoursePaging>
    </div>
  );
}
