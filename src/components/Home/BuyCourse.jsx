import React from "react";
import { BuyCourseStyle } from "../../styles/BuyCourse.Styled";

export default function BuyCourse() {
  return (
    <BuyCourseStyle
      className="text-center"
      style={{
        backgroundImage: "url('./images/group-3.png')",
        backgroundRepeat: "no-repeat",
        backgroundPosition: "top center",
      }}
    >
      <h4 className="title ">
        Đăng ký khóa học <br />
        qua 4 bước
      </h4>
      <div className="content">
        <div className="step-group">
          <div>
            BƯỚC
            <br />1
          </div>
          <p>
            Lựa chọn khóa
            <br />
            học
          </p>
        </div>
        <div className="line"></div>
        <div className="step-group">
          <div>
            BƯỚC
            <br />2
          </div>
          <p>
            Đăng nhập
            <br />
            tài khoản
          </p>
        </div>
        <div className="line"></div>
        <div className="step-group">
          <div>
            BƯỚC
            <br />3
          </div>
          <p>
            Thanh toán
            <br />
            khóa học
          </p>
        </div>
        <div className="line"></div>
        <div className="step-group">
          <div>
            BƯỚC
            <br />4
          </div>
          <p>
            Kiểm tra
            <br />
            khóa học
          </p>
        </div>
      </div>
    </BuyCourseStyle>
  );
}
