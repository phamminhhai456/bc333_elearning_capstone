import React from "react";
import CountUp from "react-countup";
import {
  InfoCourseContent,
  InfoCourseOverFlow,
  InfoCourseStyle,
} from "../../styles/InfoCourse.Styled";

export default function InfoCourse() {
  return (
    <InfoCourseStyle style={{ backgroundImage: "url('./images/info-bg.jpg')" }}>
      <div
        className="ie-overflow"
        style={{ backgroundImage: "url('./images/info-bg-2.png')" }}
      ></div>
      <div className="container">
        <div className="ie-content">
          <div className="row">
            <div className="icon-group col-4">
              <CountUp
                start={0}
                end={150}
                duration={5.75}
                style={{ fontSize: 75 }}
                delay={0.5}
              ></CountUp>

              <div>Giáo viên</div>
            </div>
            <div className="icon-group col-4">
              <CountUp
                start={0}
                end={350}
                duration={5.75}
                style={{ fontSize: 75 }}
                delay={0.5}
              ></CountUp>
              <div>Bài giảng</div>
            </div>
            <div className="icon-group col-4">
              <CountUp
                start={0}
                end={900}
                duration={5.75}
                style={{ fontSize: 75 }}
                delay={0.5}
              ></CountUp>
              <div>Học viên</div>
            </div>
          </div>
        </div>
      </div>
    </InfoCourseStyle>
  );
}
