import React from "react";
import { FooterStyle } from "../../styles/Footer.Styled";

export default function Footer() {
  return (
    <FooterStyle
      className="footer"
      style={{ backgroundImage: " url('../../../images/footer.jpg')" }}
    >
      <div className="overflow" />
      <div className="container">
        <div className="main-content">
          <div className="row">
            <div className="col-4 chinh-sach">
              <h5>Chính sách &amp; quy định</h5>
              <ol className="list-unstyled">
                <li>
                  <a href="#">Thỏa thuận sử dụng</a>
                </li>
                <li>
                  <a href="#">Quy chế hoạt động</a>
                </li>
                <li>
                  <a href="#">Chính sách bảo mật</a>
                </li>
                <li>
                  <a href="#">Quyền lợi thành viên</a>
                </li>
              </ol>
            </div>
            <div className="lien-ket col-4 text-center">
              <h5>Liên kết</h5>
              <div className="d-flex justify-content-center">
                <a className="facebook">
                  <i className="fab fa-facebook" aria-hidden="true" />
                </a>
                <a className="youtube">
                  <i className="fab fa-youtube" aria-hidden="true" />
                </a>
              </div>
            </div>
            <div className="col-4 lien-he text-left">
              <h5 className="d-flex justify-content-center">Liên hệ</h5>
              <ol className="list-unstyled">
                <li>
                  <i className="fa fa-envelope" aria-hidden="true">
                    <span>examples@gmail.com</span>
                  </i>
                </li>
                <li>
                  <i className="fa fa-phone" aria-hidden="true">
                    <span>0123456789</span>
                  </i>
                </li>
                <li>
                  <i className="fa fa-envelope" aria-hidden="true">
                    <span>examples@yahoo.com</span>
                  </i>
                </li>

                <li>
                  <i className="fa fa-phone" aria-hidden="true">
                    <span>097654321</span>
                  </i>
                </li>
              </ol>
            </div>
          </div>
        </div>
        <div className="line-mid" />
        <div className="second-content d-flex align-content-center">
          <p className="justify-content-between">
            Copyright © Project: E-Learning 2022. All rights reserved.
            <div className="text-center">
              Created by{" "}
              <a href="https://www.facebook.com/profile.php?id=100045272527920&sk=about">
                Phạm Minh Hải
              </a>{" "}
              and{" "}
              <a href="https://www.facebook.com/tqthang91/">Trần Quang Thắng</a>
            </div>
          </p>
        </div>
      </div>
    </FooterStyle>
  );
}
