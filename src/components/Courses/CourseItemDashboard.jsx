import { Button, Popconfirm, Rate, Space, Tag } from "antd";
import React, { useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import {
  showNotification,
  typeNotify,
  typePlacement,
} from "../shared/Notification";
import { actDeleteCourseCreationAsync } from "../../redux/actions/courseAction";
import {
  CardStyled,
  SpaceCategoryAndPrice,
  WapperCardItem,
} from "../../styles/Card.Styled";
import {
  EyeOutlined,
  DeleteFilled,
  CloseCircleOutlined,
} from "@ant-design/icons";
import CourseInfoModal from "./CourseInfoModal";
export default function CourseItemDashboard({
  course,
  isShowCreation = false,
}) {
  const dispatch = useDispatch();
  const [isModalVisible, setIsModalVisible] = useState(false);

  const currentUser = useSelector((state) => state.Auths.currentUser);

  const showModal = () => {
    setIsModalVisible(true);
  };

  const handleCancel = () => {
    setIsModalVisible(false);
  };
  function confirmDeleteCourse() {
    dispatch(
      actDeleteCourseCreationAsync(
        course?.maKhoaHoc,
        course?.danhMucKhoaHoc?.maDanhMucKhoahoc
      )
    ).then(function (res) {
      if (res.ok) {
        showNotification({
          type: typeNotify.success,
          placement: typePlacement.bottomLeft,
          message: res.message,
        });
      } else {
        showNotification({
          type: typeNotify.error,
          placement: typePlacement.bottomLeft,
          message: res.message,
        });
      }
    });
  }

  const fullNamAuhtor = isShowCreation
    ? currentUser?.hoTen
    : course?.nguoiTao?.hoTen;

  return (
    <WapperCardItem>
      <CardStyled
        hoverable
        cover={<img alt={course?.biDanh} src={course?.hinhAnh} />}
      >
        {/* Information */}
        <h3 className="title">{course?.tenKhoaHoc}</h3>
        <p className="author">{fullNamAuhtor || "Anonymous"}</p>

        {/* rate */}
        <div className="rate">
          <div className="star">
            <span className="num_star">4.5</span>
            <Rate disabled allowHalf defaultValue={4.5} />
          </div>
          <div className="view">
            <EyeOutlined />
            <span>{course?.luotXem}</span>
          </div>
        </div>
        {/* Category */}

        <SpaceCategoryAndPrice>
          <Tag color="orange" className="category">
            {course?.danhMucKhoaHoc?.tenDanhMucKhoaHoc || "Coding"}
          </Tag>
          <div className="price">
            <span className="free">Free</span>
            <span className="pay">$89.99</span>
          </div>
        </SpaceCategoryAndPrice>

        {/* action */}

        <Space className="action_creation">
          <Button
            type="default"
            onClick={showModal}
            className="view_detail"
            block
          >
            Xem chi tiết
          </Button>

          <Popconfirm
            placement="top"
            title="Bạn muốn xóa khóa học này không ?"
            onConfirm={confirmDeleteCourse}
            okText="Xóa"
            cancelText="Hủy"
            icon={<CloseCircleOutlined style={{ color: "red" }} />}
          >
            <div className="delete_course">
              <DeleteFilled />
            </div>
          </Popconfirm>
        </Space>
      </CardStyled>

      <CourseInfoModal
        course={course}
        isModalVisible={isModalVisible}
        handleCancel={handleCancel}
      />
    </WapperCardItem>
  );
}
