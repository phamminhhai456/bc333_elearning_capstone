import { Avatar, List } from "antd";
import React from "react";
import { useDispatch, useSelector } from "react-redux";
import { Link, useHistory } from "react-router-dom";
import { actLogout } from "../../redux/actions/authAction";
import {
  ListUserHeader,
  PopoverWrapperUserHeader,
} from "../../styles/Header.Styled";
import {
  LogoutOutlined,
  ReadOutlined,
  SettingOutlined,
} from "@ant-design/icons";
import AvatarDefault from "../../assets/images/avata_default.png";
export default function HeaderUser() {
  const history = useHistory();
  const currentUser = useSelector((state) => state.Auths.currentUser);

  function handleLogout() {
    localStorage.clear();
    history.push("/");
    window.location.reload("/");
  }

  return (
    <PopoverWrapperUserHeader
      placement="bottomRight"
      content={
        <ListUserHeader size="small">
          <List.Item className="courses">
            <Link to="/dashboard">
              <ReadOutlined />
              <span>Dashboard</span>
            </Link>
          </List.Item>
          <List.Item className="setting">
            <Link to="/setting">
              <SettingOutlined />
              <span>Setting</span>
            </Link>
          </List.Item>
          <List.Item className="logout" onClick={handleLogout}>
            <LogoutOutlined />
            <span>Logout</span>
          </List.Item>
        </ListUserHeader>
      }
      trigger="hover"
    >
      <div className="avatar">
        <Avatar size="default" alt="user_avatar" src={AvatarDefault} />
        <span className="user_name">{currentUser?.taiKhoan}</span>
      </div>
    </PopoverWrapperUserHeader>
  );
}
