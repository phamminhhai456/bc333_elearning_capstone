import { Avatar, Divider, List, Tooltip } from "antd";
import React, { Fragment } from "react";
import { LoginOutlined } from "@ant-design/icons";
import { useSelector } from "react-redux";
import { Link, useHistory, useRouteMatch } from "react-router-dom";
import {
  ContainerHeader,
  HeaderTop,
  ListUserHeader,
  PopoverWrapperUserHeader,
  SpaceStyled,
  WrapperHeaderTop,
} from "../../styles/Header.Styled";
import {
  LogoutOutlined,
  ReadOutlined,
  SettingOutlined,
} from "@ant-design/icons";
import HeaderLogo from "./HeaderLogo";
import HeaderCategories from "./HeaderCategories";
import HeaderSearch from "./HeaderSearch";
import HeaderMyCourse from "./HeaderMyCourse";
import HeaderUser from "./HeaderUser";
import AvatarDefault from "../../assets/images/avata_default.png";
export default function Header() {
  const currentUser = useSelector((state) => state.Auths.currentUser);

  const isMatchDashboard = useRouteMatch("/dashboard");

  return (
    <ContainerHeader isDashboard={Boolean(isMatchDashboard)}>
      <HeaderTop>
        <WrapperHeaderTop>
          <div className="headerTop__right">
            <HeaderLogo />
            <HeaderCategories />
            {!isMatchDashboard ? <HeaderSearch /> : null}
          </div>
          <SpaceStyled size="small" split={<Divider type="vertical" />}>
            <HeaderMyCourse />
            {currentUser?.taiKhoan === undefined ? (
              <Tooltip
                placement="bottomRight"
                title="Đăng nhập"
                mouseEnterDelay={0}
                mouseLeaveDelay={0}
              >
                <Link to="/login" className="login">
                  <LoginOutlined />
                </Link>
              </Tooltip>
            ) : (
              <HeaderUser />
            )}
          </SpaceStyled>
        </WrapperHeaderTop>
      </HeaderTop>
    </ContainerHeader>
  );
}
