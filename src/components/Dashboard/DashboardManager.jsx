import { Col, Row } from "antd";
import React, { useState } from "react";
import { Route, Switch, useRouteMatch } from "react-router-dom";
import {
  ColManagerCourse,
  RowManagerCategoryUser,
} from "../../styles/Dashboard.Styled";
import CourseCreation from "../CourseCreation/CourseCreation";
import CourseUpload from "../CourseUpload/CourseUpload";

import ManagerCategories from "./ManagerCategories";
import ManagerCourse from "./ManagerCourse";
import ManagerUsers from "./ManagerUsers";

export default function DashboardManager() {
  let { path } = useRouteMatch();
  const [containerAffix, setContainerAffix] = useState(null);
  return (
    <div style={{ padding: "0px 20px" }}>
      <Row gutter={[16, 16]}>
        <ColManagerCourse span={16}>
          <Switch>
            <Route exact path={path}>
              <ManagerCourse />
            </Route>
            <Route path={`${path}/course-creation`} exact>
              <CourseCreation />
            </Route>
            <Route path={`${path}/course-upload`} exact>
              <CourseUpload />
            </Route>
          </Switch>
        </ColManagerCourse>
        {/* Sidebar */}
        <Col span={8}>
          <RowManagerCategoryUser gutter={[0, 16]} ref={setContainerAffix}>
            <Col span={24}>
              <ManagerCategories />
            </Col>

            <Col span={24}>
              <ManagerUsers containerAffix={containerAffix} />
            </Col>
          </RowManagerCategoryUser>
        </Col>
      </Row>
    </div>
  );
}
