import { api } from "./url.service";
export const CategoriesService = {
  GetList() {
    return api.call().get("/QuanLyKhoaHoc/LayDanhMucKhoaHoc");
  },
};
