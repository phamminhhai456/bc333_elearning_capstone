import { api, https } from "./url.service";

export const AuthServices = {
  Register({ taiKhoan, matKhau, hoTen, soDT, email }) {
    return api.call().post("/QuanLyNguoiDung/DangKy", {
      taiKhoan,
      matKhau,
      hoTen,
      soDT,
      email,
      maLoaiNguoiDung: "GV",
      maNhom: "GP01",
    });
  },
  Login({ taiKhoan, matKhau }) {
    return api.call().post("/QuanLyNguoiDung/DangNhap", { taiKhoan, matKhau });
  },
  getInfoCurrentUser() {
    return api.callWithAuth().post("/QuanLyNguoiDung/ThongTinNguoiDung");
  },
  postRegister: ({ taiKhoan, matKhau, hoTen, soDT, email }) => {
    let uri = "/QuanLyNguoiDung/DangKy";
    return https.post(uri, {
      taiKhoan,
      matKhau,
      hoTen,
      soDT,
      email,
      maLoaiNguoiDung: "GV",
      maNhom: "GP01",
    });
  },
  postLogin: (dataLogin) => {
    let uri = "/QuanLyNguoiDung/DangNhap";
    return https.post(uri, dataLogin);
  },
};
