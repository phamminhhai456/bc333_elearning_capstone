import { CategoriesService } from "../../services/categories.service";
import { GET_LIST_CATEGORIES } from "../constants/categoriesConstants";
export function actGetListCategories(list) {
  return {
    type: GET_LIST_CATEGORIES,
    payload: {
      list,
    },
  };
}

export function actGetListCategoriesAsync() {
  return async function (dispatch) {
    try {
      const response = await CategoriesService.GetList();
      if (response.status === 200) {
        const list = response.data;
        dispatch(actGetListCategories(list));
      }
    } catch (error) {
      console.log(error);
    }
  };
}
