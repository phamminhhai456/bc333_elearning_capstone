import {
  ACCESS_TOKEN,
  DECREASE_MY_COURSES,
  GET_CURRENT_USER,
  INCREASE_MY_COURSES,
  LOG_OUT,
  SAVE_INFO_CURRENT_USER,
  SAVE_TOKEN,
  SAVE_USER,
  SET_LOGIN,
} from "../constants/authConstant";
import { AuthServices } from "../../services/auth.service";
import { USER_INFOR } from "../../services/local.service";
import { message } from "antd";
// import { message } from "antd";
// import { history } from "../../index";
export function actDecreaseCourse({ maKhoaHoc }) {
  // giam dan
  return {
    type: DECREASE_MY_COURSES,
    payload: {
      maKhoaHoc,
    },
  };
}
export function actIncreaseCourse({ maKhoaHoc, tenKhoaHoc }) {
  return {
    type: INCREASE_MY_COURSES,
    payload: {
      maKhoaHoc,
      tenKhoaHoc,
    },
  };
}
export function actGetCurrentUser(user) {
  return {
    type: GET_CURRENT_USER,
    payload: {
      user,
    },
  };
}

export function actSaveToken(token) {
  return {
    type: SAVE_TOKEN,
    payload: {
      token,
    },
  };
}

export function actSaveInfoCurrentUser(user) {
  return {
    type: SAVE_INFO_CURRENT_USER,
    payload: {
      user,
    },
  };
}
export function actLogout() {
  return {
    type: LOG_OUT,
  };
}

export function actRegisterAsync({ taiKhoan, matKhau, hoTen, soDT, email }) {
  return async function (dispatch) {
    try {
      const response = await AuthServices.Register({
        taiKhoan,
        matKhau,
        hoTen,
        soDT,
        email,
      });
      console.log(response);
      if (response.status === 200) {
        // uesrInfo= {taiKhoan, matKhau, hoTen, soDT, email, maNhom}
        const userInfo = response.data;
        return {
          ok: true,
          user: userInfo,
        };
      } else {
        return {
          ok: false,
        };
      }
    } catch (error) {
      console.log(error);
      return {
        ok: false,
        error: { error },
      };
    }
  };
}

export function actGetInfoCurrentUserAsync() {
  return async function (dispatch) {
    try {
      const response = await AuthServices.getInfoCurrentUser();

      if (response.status === 200) {
        dispatch(actSaveInfoCurrentUser(response.data));
      }
    } catch (error) {}
  };
}

export function actLoginAsync({ taiKhoan, matKhau }) {
  return async function (dispatch) {
    try {
      const response = await AuthServices.Login({
        taiKhoan,
        matKhau,
      });

      if (response.status === 200) {
        const { accessToken, ...user } = response.data;

        dispatch(actSaveToken(accessToken));
        await dispatch(actGetInfoCurrentUserAsync());
        return {
          ok: true,
          user,
        };
      } else {
        return {
          ok: false,
        };
      }
    } catch (error) {
      return {
        ok: false,
        message: error?.response?.data,
      };
    }
  };
}

export function actCheckLoginAsync() {
  return async function (dispatch) {
    try {
      const token = Storage.getToken();
      if (token && token !== "") {
        dispatch(actSaveToken(token));
        await dispatch(actGetInfoCurrentUserAsync());
      }
    } catch (error) {}
  };
}

export const actLoginApi = (values) => {
  return (dispatch) => {
    AuthServices.postLogin(values)
      .then((res) => {
        console.log(res);
        if (res.status === 200) {
          localStorage.setItem(USER_INFOR, JSON.stringify(res.data));
          localStorage.setItem(
            ACCESS_TOKEN,
            JSON.stringify(res.data.accessToken)
          );
          dispatch({
            type: SET_LOGIN,
            payload: res.data.content,
          });
        }
      })
      .catch((err) => {
        message.error(err.response.data.content);
        console.log(err);
      });
  };
};
