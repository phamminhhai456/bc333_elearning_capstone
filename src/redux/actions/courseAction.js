import { SwapCalls } from "@mui/icons-material";
import axios from "axios";
import Swal from "sweetalert2";
import { CourseService } from "../../services/course.service";
import {
  ACT_DELETE_COURSE_CREATION,
  ACT_GET_LIST_COURSE,
  ACT_GET_LIST_COURSE_CATEGORIES,
  ACT_GET_LIST_ALL,
  ACT_GET_LIST_COURSE_SEARCH,
} from "../constants/courseConstant";
import { actIncreaseCourse, actDecreaseCourse } from "./authAction";
export function actDeleteCourseCreation(maKhoaHoc, maDanhMuc) {
  return {
    type: ACT_DELETE_COURSE_CREATION,
    payload: {
      maKhoaHoc,
      maDanhMuc,
    },
  };
}
export function actGetListCourse(
  { count, currentPage, list, totalCount, totalPages },
  typeAction = ACT_GET_LIST_COURSE
) {
  return {
    type: typeAction,
    payload: {
      list,
      count,
      currentPage,
      totalCount,
      totalPages,
    },
  };
}

export function actGetListCourseCategories({ maDanhMuc, list }) {
  return {
    type: ACT_GET_LIST_COURSE_CATEGORIES,
    payload: {
      maDanhMuc,
      list,
    },
  };
}

export function actGetListCourseAll(list) {
  return {
    type: ACT_GET_LIST_ALL,
    payload: {
      list,
    },
  };
}
export function actGetListCourseAllAsync() {
  return async function (dispatch) {
    try {
      const response = await CourseService.GetListCourses();
      dispatch(actGetListCourseAll(response.data));
    } catch (error) {}
  };
}
export function actGetListCourseAsync({ page = 1, pageSize = 8 } = {}) {
  return async function (dispatch) {
    try {
      const response = await CourseService.GetLlistCoursePagination({
        page,
        pageSize,
      });
      if (response.status === 200) {
        const {
          count,
          currentPage,
          items: list,
          totalCount,
          totalPages,
        } = response.data;
        dispatch(
          actGetListCourse({
            list,
            count,
            currentPage,
            totalCount,
            totalPages,
          })
        );
        return {
          ok: true,
          list,
        };
      } else {
        return {
          ok: false,
        };
      }
    } catch (error) {
      return {
        ok: false,
        error,
      };
    }
  };
}

export function actGetListCourseByCategoryAsync(maDanhMuc) {
  return async function (dispatch) {
    try {
      const response = await CourseService.GetListCourseByCategory({
        maDanhMuc,
      });

      if (response.status === 200) {
        dispatch(
          actGetListCourseCategories({
            maDanhMuc,
            list: response.data,
          })
        );
        return {
          ok: true,
          data: response.data,
        };
      }
    } catch (error) {
      return {
        ok: false,
        message: error?.response?.data,
      };
    }
  };
}

export function actGetListCourseBySearchAsync({
  page = 1,
  pageSize = 4,
  tenKhoaHoc = "",
} = {}) {
  return async function (dispatch) {
    try {
      const response = await CourseService.GetListCourseSearch({
        page,
        pageSize,
        tenKhoaHoc,
      });

      if (response.status === 200) {
        const {
          currentPage,
          count,
          totalPages,
          totalCount,
          items: list,
        } = response.data;

        dispatch(
          actGetListCourse(
            {
              list,
              count,
              currentPage,
              totalCount,
              totalPages,
            },
            ACT_GET_LIST_COURSE_SEARCH
          )
        );

        return {
          ok: true,
        };
      }
    } catch (error) {
      if (error.response.status === 500) {
        dispatch(
          actGetListCourse(
            {
              list: [],
              count: 0,
              currentPage: 1,
              totalCount: 0,
              totalPages: 1,
            },
            ACT_GET_LIST_COURSE_SEARCH
          )
        );
      }
      return {
        ok: false,
        message: error?.response?.data,
      };
    }
  };
}

export function actRegisterCourseAsync({ taiKhoan, maKhoaHoc, tenKhoaHoc }) {
  return async function (dispatch) {
    try {
      const response = await CourseService.RegisterCourse({
        taiKhoan,
        maKhoaHoc,
      });

      if (response.status === 200) {
        dispatch(actIncreaseCourse({ maKhoaHoc, tenKhoaHoc }));
        return {
          ok: true,
          message: response.data,
        };
      }
    } catch (error) {
      return {
        ok: false,
        message: error?.response?.data,
      };
    }
  };
}

export function actDeleteRegisterCourseAsync({ taiKhoan, maKhoaHoc }) {
  return async function (dispatch) {
    try {
      const response = await CourseService.DeleteRegisterCourse({
        taiKhoan,
        maKhoaHoc,
      });

      if (response.status === 200) {
        dispatch(actDecreaseCourse({ maKhoaHoc }));
        return {
          ok: true,
          message: response.data,
        };
      }
    } catch (error) {
      console.log({ error });
      return {
        ok: false,
        message: error?.response?.data,
      };
    }
  };
}

export function actCreateNewCourseAsync({
  maKhoaHoc,
  tenKhoaHoc,
  moTa,
  maNhom = "GP01",
  ngayTao,
  maDanhMucKhoaHoc,
  taiKhoanNguoiTao,
  hinhAnh,
  biDanh = "bi-danh",
  luotXem = 100,
  danhGia = 10,
} = {}) {
  return async function (dispatch) {
    try {
      const formData = new FormData();
      formData.append("moTa", moTa);
      formData.append("biDanh", biDanh);
      formData.append("luotXem", luotXem);
      formData.append("danhGia", danhGia);
      formData.append("maNhom", maNhom);
      formData.append("ngayTao", ngayTao);
      formData.append("hinhAnh", hinhAnh);
      formData.append("maKhoaHoc", maKhoaHoc);
      formData.append("tenKhoaHoc", tenKhoaHoc);
      formData.append("maDanhMucKhoaHoc", maDanhMucKhoaHoc);
      formData.append("taiKhoanNguoiTao", taiKhoanNguoiTao);

      const response = await CourseService.CreateNewCourse(formData);

      if (response.status === 200) {
        const newCourse = response.data;
        await dispatch(
          actGetListCourseByCategoryAsync(newCourse.maDanhMucKhoaHoc)
        );
        await dispatch(actGetListCourseAllAsync());
        return {
          ok: true,
          course: newCourse,
        };
      }
    } catch (error) {
      console.log(error);
      return {
        ok: false,
      };
    }
  };
}

export function actUploadCourseAsync({
  maKhoaHoc,
  tenKhoaHoc,
  moTa,
  maNhom = "GP01",
  ngayTao,
  maDanhMucKhoaHoc,
  taiKhoanNguoiTao,
  hinhAnh,
  biDanh = "",
  luotXem = 100,
  danhGia = 10,
} = {}) {
  return async function (dispatch) {
    try {
      const formData = new FormData();

      formData.append("hinhAnh", hinhAnh);

      formData.append("tenKhoaHoc", tenKhoaHoc);

      if (typeof hinhAnh === "object") {
        const responseImg = await CourseService.UploadHinhAnhKhoaHoc(formData);
      }

      const response = await CourseService.UploadCourseStringBody({
        maKhoaHoc,
        tenKhoaHoc,
        moTa,
        maNhom,
        ngayTao,
        maDanhMucKhoaHoc,
        taiKhoanNguoiTao,
        hinhAnh,
        biDanh,
        luotXem,
        danhGia,
      });
      console.log(response);
      if (response.status === 200) {
        await dispatch(actGetListCourseAllAsync());

        return {
          ok: true,
        };
      } else {
        return {
          ok: false,
        };
      }
    } catch (error) {
      console.log("error upload course", { error });
    }
  };
}

export function actDeleteCourseCreationAsync(maKhoaHoc, maDanhMuc) {
  return async function (dispatch) {
    try {
      const response = await CourseService.DeleteCourseCreation(maKhoaHoc);
      if (response.status === 200) {
        dispatch(actDeleteCourseCreation(maKhoaHoc, maDanhMuc));
        return {
          ok: true,
          message: response.data,
        };
      }
    } catch (error) {
      return {
        ok: false,
        message: error?.response?.data,
      };
    }
  };
}
export const additionCouresAction = (value, token, hinhAnh) => {
  return async () => {
    try {
      let frm = new FormData();
      let {
        maKhoaHoc,
        tenKhoaHoc,
        moTa,
        luotXem,
        danhGia,
        maNhom,
        ngayTao,
        maDanhMucKhoaHoc,
        taiKhoanNguoiTao,
        biDanh,
      } = value;
      frm.append("file", hinhAnh);
      // frm.append("value", value);
      frm.append("tenKhoaHoc", tenKhoaHoc);
      frm.append("maKhoaHoc", maKhoaHoc);
      frm.append("moTa", moTa);
      frm.append("luotXem", luotXem);
      frm.append("danhGia", danhGia);
      // frm.append("hinhAnh", { hinhAnh: value.hinhAnh });
      frm.append("maNhom", maNhom);
      frm.append("ngayTao", ngayTao);
      frm.append("taiKhoanNguoiTao", taiKhoanNguoiTao);
      frm.append("maDanhMucKhoaHoc", maDanhMucKhoaHoc);
      frm.append("biDanh", biDanh);
      const result = await axios({
        url: "https://elearning0706.cybersoft.edu.vn/api/QuanLyKhoaHoc/ThemKhoaHocUploadHinh",
        method: "POST",
        data: frm,
        headers: {
          Authorization: "Bearer  " + token,
        },
      });
      alert("Thêm Khóa Học Thành Công");
      Swal.fire({
        icon: "success",
        title: "Thêm Khóa Học Thành Công",
        text: `Khóa Học + ${tenKhoaHoc} đăng ký thành công`,
      });
      console.log(result);
    } catch (err) {
      console.log(err.response?.data);
      Swal.fire({
        icon: "success",
        title: "Thêm Khóa Học Thất Bại",
      });
    }
  };
};
