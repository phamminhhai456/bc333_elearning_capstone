import { combineReducers } from "redux";
import { authReducer } from "./authReducer";
import { userReducer } from "./userReducer";
import { CategoriesReducer } from "./categoriesReducer";
import { courseReducer } from "./courseReducer";
const rootReducer = combineReducers({
  Auths: authReducer,
  User: userReducer,
  Categories: CategoriesReducer,
  Courses: courseReducer,
});
export default rootReducer;
