import { GET_LIST_CATEGORIES } from "../constants/categoriesConstants";

const initialState = {
  list: [],
};

export const CategoriesReducer = (stateCategoris = initialState, action) => {
  switch (action.type) {
    case GET_LIST_CATEGORIES: {
      const { list } = action.payload;
      return {
        ...stateCategoris,
        list,
      };
    }

    default: {
      return stateCategoris;
    }
  }
};
